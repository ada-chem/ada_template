"""
The `config.py` file contains settings for the current project and imports and sets up any
required profiles, components and assets.
"""

from ada_core.profiles.vials import HPLC_2mL_pierce
from ada_core.profiles.trays import vial_tray_gen1, needle_tray_gen2
from ada_core.profiles.tip_removers import integrated_gen2
from ada_core.components.manipulated import Tray, TipRemover
from ada_core.dependencies.simulator import Asset

#
# Settings
#

ROBOTARM_VELOCITY = 30000
ROBOTARM_SAFE_POSITION = {'x': 0, 'y': 100, 'z': 280}

#
# Assets
#

# a 100mm cube
cube_asset = {
    'type': 'cube',
    'model_scale': {'x': 100, 'y': 100, 'z': 100}
}

#
# Profiles
#

vial_profile = {
    # use built-in profile for default profile values
    **HPLC_2mL_pierce,
    # override any default profile values
    **{
        # ...
    }
}

vial_tray_profile = {
    # use built-in vial tray for default values
    **vial_tray_gen1,
    # override any default vial tray profile values here
    **{
        'itemclass': 'Vial',
        'itemkwargs': vial_profile,
    }
}

needle_profile = {
    'gauge': 21,
    'bd_length': 50
}

needle_tray_profile = {
    # use built-in profile for default profile values
    **needle_tray_gen2,
    # override any default profile values
    **{
        'itemclass': 'Needle',
        'itemkwargs': needle_profile,
    }
}

needle_tip_remover_profile = {
    # use built-in profile for default profile values
    **integrated_gen2,
    # override any default profile values
    **{
        # ...
    }
}

#
# Components
#

vial_tray = Tray(
    location='O5',
    **vial_tray_profile,
)

needle_tray = Tray(
    location='C6',
    **needle_tray_profile
)

needle_tip_remover = TipRemover(
    location='C6',
    **needle_tip_remover_profile
)

cube = Asset('E10', cube_asset)
