"""
The `sequences.py` file contains methods that expose a set of robot movements as a simple
high-level "sequence". These sequences should start and end in a safe position for the robot arm.
"""

import time
from ada_core.dependencies.arm import robotarm
from config import *


def home():
    # wait a few seconds so the simulator has time to load our assets
    time.sleep(5)
    robotarm.home()


def safe_position():
    robotarm.move_to_location(ROBOTARM_SAFE_POSITION, splitxyz='zxy')


def pickup_new_needle():
    safe_position()
    needle = needle_tray.get_item()
    needle.retrieve_tip()
    safe_position()


def remove_needle():
    safe_position()
    needle_tip_remover.remove_tip()
    safe_position()


def pierce_vial():
    safe_position()
    vial = vial_tray.get_item()
    pierce_location = vial.get_pierce_location()
    robotarm.move_to_location(pierce_location, target='probe')
    safe_position()
