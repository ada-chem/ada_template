Ada Project Template
====================

This is a simple project template that should make it quick and easy to start developing a project
using `ada_core` and the North Robotic's N9 robot.

Getting Started
---------------

### North Robotics Editor

1. Click the _Install Requirements_ (![package icon][package]) button on the toolbar
1. Click the _Simulator_ (![simulator icon][simulator]) button to open the simulator in a new window
1. Open `main.py`
1. Click the _Run File_ (![run icon][run]) button to run `main.py`

[package]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.0.0/svg/package.svg
[simulator]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.0.0/svg/hubot.svg
[run]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.0.0/svg/triangle-right.svg

File Descriptions
-----------------

| file | description |
|---|---|
| main.py | The entry point to the script, calls sequence methods to drive the robot |
| config.py | Configures the robot and everything that is interacted with by the robot |
| sequences.py | Defines sequence methods that compose robot movements into useful sequences |
| robot_parameters.py | Robot-specific configuration and calibration parameters |
