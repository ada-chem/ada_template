"""
The `main.py` file is the entry point for your script. It uses the sequences defined in
`sequences.py` to build a useful script.
"""

from sequences import *

robotarm.velocity = ROBOTARM_VELOCITY

# start by homing the robot
home()

# continuously pierce vials with needles until we run out
while needle_tray.filled_locations():
    pickup_new_needle()
    pierce_vial()
    remove_needle()
